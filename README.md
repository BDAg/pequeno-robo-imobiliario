# Pequeno Robô Imobiliário 

## Descrição
Desenvolvido especificamente para investidores cujo problema é a dificuldade de monitorar as cotações de imóveis à venda. O robô imobiliário é um rastreador da web que compara o valor para aumentar os lucros por meio de análises de mudanças de preços e referências geográficas.

## [](#quais-ferramentas-foram-usadas-para-o-software)Quais ferramentas foram usadas para o software?
O Robô Imobiliário foi realizado com a linguagem Python a qual é responsável pelas funcionalidades. Foi utilizado o BeautifulSoup Para a raspagem e analise dos dados capturados.

### Linguagem de Programação:

 - [Python](https://www.python.org/) 3.8.5 ou versão maior

## Desenvolvedores 

 - [Isabelly Miranda](https://www.linkedin.com/in/isabellycaravlhomiranda)
 - Maria Fernanda Zanguetim
 - Laísa Silva


## Mentor

 - Prof. Dr. Luís Hilário Tobler Garcia
 - [Jorge Luis Zanguettin](https://www.linkedin.com/in/jorgezanguettin)
 
 Você pode assistir o pitch de nosso projeto clicando [aqui!](https://www.youtube.com/watch?v=uGJP4M--g7Q)
 

